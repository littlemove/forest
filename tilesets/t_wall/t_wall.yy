{
    "id": "45599b55-c6f4-4f18-a425-f22494fad48e",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "t_wall",
    "auto_tile_sets": [
        {
            "id": "b3c98ca2-b0b9-4be4-a147-551e4d050d8a",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": true,
            "name": "autotile_1",
            "tiles": [
                0,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                10,
                11,
                12,
                13,
                14,
                15,
                16,
                17
            ]
        }
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 4,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "a3fd0b03-2d5b-4edd-ba6f-b96d67bdd8e2",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 18,
    "tileheight": 32,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 32,
    "tilexoff": 0,
    "tileyoff": 0
}