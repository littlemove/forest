{
    "id": "5dfbb876-6216-47ea-b18d-d3fada83a9e8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player",
    "eventList": [
        {
            "id": "9c5b221b-f8f3-43d0-aa92-5065d1a0cfbf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5dfbb876-6216-47ea-b18d-d3fada83a9e8"
        },
        {
            "id": "b6a707e3-9e01-406f-b55c-3d843fa99bc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5dfbb876-6216-47ea-b18d-d3fada83a9e8"
        }
    ],
    "maskSpriteId": "1f7d5f25-1a20-4da9-882a-fd113bef01eb",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1f7d5f25-1a20-4da9-882a-fd113bef01eb",
    "visible": true
}