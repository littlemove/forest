{
    "id": "ceeae3a1-ceb8-4192-8b4e-3b8747449373",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dirtpath_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 16,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b4af356-dcc8-4481-9313-66921efd087a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceeae3a1-ceb8-4192-8b4e-3b8747449373",
            "compositeImage": {
                "id": "ca7ef539-24bd-4d64-91ce-28e7b5853d96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b4af356-dcc8-4481-9313-66921efd087a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76805a11-dc15-4ca5-aaef-39f0208b478f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b4af356-dcc8-4481-9313-66921efd087a",
                    "LayerId": "527a9437-60d4-4082-b9e2-6efce687e95b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "527a9437-60d4-4082-b9e2-6efce687e95b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ceeae3a1-ceb8-4192-8b4e-3b8747449373",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 0,
    "yorig": 0
}