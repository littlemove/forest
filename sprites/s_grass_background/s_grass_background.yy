{
    "id": "feb59b47-5eda-45d4-92df-0127d82a3e69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_grass_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3faf31bf-807d-4430-844b-bfb3d7e35fcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "feb59b47-5eda-45d4-92df-0127d82a3e69",
            "compositeImage": {
                "id": "bf50dec5-1e61-4980-80eb-f7ff47dd2872",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3faf31bf-807d-4430-844b-bfb3d7e35fcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d75b621-17e7-4536-80b0-ce601e5e8c75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3faf31bf-807d-4430-844b-bfb3d7e35fcc",
                    "LayerId": "31c13c72-0553-4ef4-94ef-fb2e40730603"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "31c13c72-0553-4ef4-94ef-fb2e40730603",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "feb59b47-5eda-45d4-92df-0127d82a3e69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}