{
    "id": "9a3557eb-a3f0-4733-b4d8-8804b71919be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "437c0968-ddc1-45b3-a96a-d25dc272e7d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a3557eb-a3f0-4733-b4d8-8804b71919be",
            "compositeImage": {
                "id": "2e46345c-e66f-48b7-b4ba-9432460eb02d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "437c0968-ddc1-45b3-a96a-d25dc272e7d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efeb5ae2-7fc2-4502-85cd-224213b952c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "437c0968-ddc1-45b3-a96a-d25dc272e7d2",
                    "LayerId": "6953bf1b-0f04-4468-8e5e-7eb277fa1e5a"
                }
            ]
        },
        {
            "id": "91780f62-1c6b-430f-95da-786ba6970de9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a3557eb-a3f0-4733-b4d8-8804b71919be",
            "compositeImage": {
                "id": "739975df-6c22-4bfc-9145-8b798b460967",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91780f62-1c6b-430f-95da-786ba6970de9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d0444fc-4fdc-4496-9628-ffa2e0ab8727",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91780f62-1c6b-430f-95da-786ba6970de9",
                    "LayerId": "6953bf1b-0f04-4468-8e5e-7eb277fa1e5a"
                }
            ]
        },
        {
            "id": "6f3ec5ae-fdc5-4fbb-8893-4f13b9005d76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a3557eb-a3f0-4733-b4d8-8804b71919be",
            "compositeImage": {
                "id": "5fe5b505-89ce-4b71-9c0e-915c006f962d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f3ec5ae-fdc5-4fbb-8893-4f13b9005d76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce879a91-5edb-4248-a553-ff5f0405d4dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f3ec5ae-fdc5-4fbb-8893-4f13b9005d76",
                    "LayerId": "6953bf1b-0f04-4468-8e5e-7eb277fa1e5a"
                }
            ]
        },
        {
            "id": "b81ae775-6337-4b9f-a2e6-f888a1332396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a3557eb-a3f0-4733-b4d8-8804b71919be",
            "compositeImage": {
                "id": "5cdc2ddd-0497-4761-8694-28a5df5a4e60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b81ae775-6337-4b9f-a2e6-f888a1332396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92f1527e-76e0-4f78-b464-04a8800aef6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b81ae775-6337-4b9f-a2e6-f888a1332396",
                    "LayerId": "6953bf1b-0f04-4468-8e5e-7eb277fa1e5a"
                }
            ]
        },
        {
            "id": "b1a66af5-9782-43f9-8bf4-289f3cf5ae54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a3557eb-a3f0-4733-b4d8-8804b71919be",
            "compositeImage": {
                "id": "7adfc1cd-4a82-47db-8f1b-3696e4524a6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1a66af5-9782-43f9-8bf4-289f3cf5ae54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7447a916-67fe-4028-bf09-e2f8f3140ff3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1a66af5-9782-43f9-8bf4-289f3cf5ae54",
                    "LayerId": "6953bf1b-0f04-4468-8e5e-7eb277fa1e5a"
                }
            ]
        },
        {
            "id": "99997fc6-5534-4a3c-a0a1-2fa6faa8e51c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a3557eb-a3f0-4733-b4d8-8804b71919be",
            "compositeImage": {
                "id": "56d0d334-ef78-45c6-a46d-ee592577ebc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99997fc6-5534-4a3c-a0a1-2fa6faa8e51c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03c51f3d-68f3-43ac-a943-d2464d5f9079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99997fc6-5534-4a3c-a0a1-2fa6faa8e51c",
                    "LayerId": "6953bf1b-0f04-4468-8e5e-7eb277fa1e5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "6953bf1b-0f04-4468-8e5e-7eb277fa1e5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a3557eb-a3f0-4733-b4d8-8804b71919be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 24
}