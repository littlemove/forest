{
    "id": "9216db9b-094d-4b24-a29b-3f954f5d73fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_run_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "670433d4-2a81-4ea7-8502-6e8d8c3fe3f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9216db9b-094d-4b24-a29b-3f954f5d73fa",
            "compositeImage": {
                "id": "122f4653-2343-4a77-ab58-669a6ff0ae8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "670433d4-2a81-4ea7-8502-6e8d8c3fe3f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "682dca14-7b23-43fd-85f6-12549cf9334e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "670433d4-2a81-4ea7-8502-6e8d8c3fe3f0",
                    "LayerId": "3cc4ba25-2f8a-4c3a-811a-708cc0353e3a"
                }
            ]
        },
        {
            "id": "59376504-f086-41e3-b291-0f730e0bc366",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9216db9b-094d-4b24-a29b-3f954f5d73fa",
            "compositeImage": {
                "id": "b269fbc7-8d67-4cab-91b2-2c74f2f4a92f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59376504-f086-41e3-b291-0f730e0bc366",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b63dc1d0-0e15-4b89-b8fe-b669b38d7bd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59376504-f086-41e3-b291-0f730e0bc366",
                    "LayerId": "3cc4ba25-2f8a-4c3a-811a-708cc0353e3a"
                }
            ]
        },
        {
            "id": "374a52b4-6927-401c-abe8-08bb591ee87e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9216db9b-094d-4b24-a29b-3f954f5d73fa",
            "compositeImage": {
                "id": "6b856588-3d5e-4b8e-8c56-0e32987c8ff7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "374a52b4-6927-401c-abe8-08bb591ee87e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3a8d91a-bcc2-4a90-ba95-fb2b3694bbd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "374a52b4-6927-401c-abe8-08bb591ee87e",
                    "LayerId": "3cc4ba25-2f8a-4c3a-811a-708cc0353e3a"
                }
            ]
        },
        {
            "id": "b4b18e0d-0a3e-43c7-98ee-44f7ad619629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9216db9b-094d-4b24-a29b-3f954f5d73fa",
            "compositeImage": {
                "id": "44a7075c-3b14-4e2a-84ed-2f08e58c2ff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4b18e0d-0a3e-43c7-98ee-44f7ad619629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2e81e30-f1be-4c01-a078-a5da2b4d21f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4b18e0d-0a3e-43c7-98ee-44f7ad619629",
                    "LayerId": "3cc4ba25-2f8a-4c3a-811a-708cc0353e3a"
                }
            ]
        },
        {
            "id": "48054ac9-5ae3-420a-8f83-220466cb04c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9216db9b-094d-4b24-a29b-3f954f5d73fa",
            "compositeImage": {
                "id": "a03e921b-6896-483b-a531-826eafca832d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48054ac9-5ae3-420a-8f83-220466cb04c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cce461b-c12d-4baf-82e1-9ba1c1a94435",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48054ac9-5ae3-420a-8f83-220466cb04c4",
                    "LayerId": "3cc4ba25-2f8a-4c3a-811a-708cc0353e3a"
                }
            ]
        },
        {
            "id": "ac057b2f-9e12-4887-a486-f7728a40aa8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9216db9b-094d-4b24-a29b-3f954f5d73fa",
            "compositeImage": {
                "id": "3a1f90b5-5260-401d-a643-e67207c5d9ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac057b2f-9e12-4887-a486-f7728a40aa8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b42814e9-1931-4ea1-83fb-d938143b8939",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac057b2f-9e12-4887-a486-f7728a40aa8d",
                    "LayerId": "3cc4ba25-2f8a-4c3a-811a-708cc0353e3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "3cc4ba25-2f8a-4c3a-811a-708cc0353e3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9216db9b-094d-4b24-a29b-3f954f5d73fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 24
}