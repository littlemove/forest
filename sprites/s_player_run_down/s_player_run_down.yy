{
    "id": "1f7d5f25-1a20-4da9-882a-fd113bef01eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_run_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a08e726e-7b80-4334-8ee2-db2dd68de5c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f7d5f25-1a20-4da9-882a-fd113bef01eb",
            "compositeImage": {
                "id": "48e0e240-1bd2-4338-8b1c-5c386b259277",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a08e726e-7b80-4334-8ee2-db2dd68de5c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be78fd04-b1de-49c6-96ba-d898763b59e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a08e726e-7b80-4334-8ee2-db2dd68de5c2",
                    "LayerId": "e869af57-3172-4600-aee7-6ca49fb4184d"
                }
            ]
        },
        {
            "id": "9e2a71b9-43f6-40cc-a94b-43cf5949ced7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f7d5f25-1a20-4da9-882a-fd113bef01eb",
            "compositeImage": {
                "id": "3e208a95-74b9-42d6-955e-bf979b0263b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e2a71b9-43f6-40cc-a94b-43cf5949ced7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33be631d-12a0-4be7-bf64-16d4af73083d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e2a71b9-43f6-40cc-a94b-43cf5949ced7",
                    "LayerId": "e869af57-3172-4600-aee7-6ca49fb4184d"
                }
            ]
        },
        {
            "id": "fb37028f-1f9f-43e4-beca-2051c3fbd139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f7d5f25-1a20-4da9-882a-fd113bef01eb",
            "compositeImage": {
                "id": "8ec12709-9e69-47fb-bb7c-4501dfa83edd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb37028f-1f9f-43e4-beca-2051c3fbd139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdd40cc7-4201-4714-a6d4-4f38df54a9f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb37028f-1f9f-43e4-beca-2051c3fbd139",
                    "LayerId": "e869af57-3172-4600-aee7-6ca49fb4184d"
                }
            ]
        },
        {
            "id": "13513e5d-1ff1-4205-b0aa-8bb2872a40db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f7d5f25-1a20-4da9-882a-fd113bef01eb",
            "compositeImage": {
                "id": "fccc9e51-fb3c-4c3e-9f00-8e153e7f03e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13513e5d-1ff1-4205-b0aa-8bb2872a40db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd153889-f6af-414a-a9aa-e4da0e3f3899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13513e5d-1ff1-4205-b0aa-8bb2872a40db",
                    "LayerId": "e869af57-3172-4600-aee7-6ca49fb4184d"
                }
            ]
        },
        {
            "id": "8d86046d-6d22-4059-8f8d-5f60e932ba89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f7d5f25-1a20-4da9-882a-fd113bef01eb",
            "compositeImage": {
                "id": "7c751bb9-3396-43bd-888f-38966d1f7c04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d86046d-6d22-4059-8f8d-5f60e932ba89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27bf8a10-469e-478d-953a-dfbc8f60d693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d86046d-6d22-4059-8f8d-5f60e932ba89",
                    "LayerId": "e869af57-3172-4600-aee7-6ca49fb4184d"
                }
            ]
        },
        {
            "id": "0fb1ef9f-2c69-4d52-afb5-95ef4dc907ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f7d5f25-1a20-4da9-882a-fd113bef01eb",
            "compositeImage": {
                "id": "d11f2d9d-3ce4-4574-a926-6055136c7dc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fb1ef9f-2c69-4d52-afb5-95ef4dc907ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "782c2d44-34b8-40ce-8f72-2e835ee2cba8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fb1ef9f-2c69-4d52-afb5-95ef4dc907ad",
                    "LayerId": "e869af57-3172-4600-aee7-6ca49fb4184d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "e869af57-3172-4600-aee7-6ca49fb4184d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f7d5f25-1a20-4da9-882a-fd113bef01eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 24
}