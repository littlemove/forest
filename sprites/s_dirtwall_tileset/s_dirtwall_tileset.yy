{
    "id": "a3fd0b03-2d5b-4edd-ba6f-b96d67bdd8e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dirtwall_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 32,
    "bbox_right": 287,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "962cedf8-a885-4431-a449-8cfb0e46e8d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3fd0b03-2d5b-4edd-ba6f-b96d67bdd8e2",
            "compositeImage": {
                "id": "00aa280d-8cba-4689-aefe-47ae6f32635e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "962cedf8-a885-4431-a449-8cfb0e46e8d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbc396e9-fd14-4e89-857b-1024763ce52e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "962cedf8-a885-4431-a449-8cfb0e46e8d6",
                    "LayerId": "ec8a0284-36ac-4789-a37a-90cc9d48e81d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ec8a0284-36ac-4789-a37a-90cc9d48e81d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3fd0b03-2d5b-4edd-ba6f-b96d67bdd8e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 288,
    "xorig": 0,
    "yorig": 0
}