{
    "id": "eef9b7f7-d0aa-42a2-9f37-00ba16543895",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b34cdd5-668c-45c0-af8e-c2d8a840a16a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef9b7f7-d0aa-42a2-9f37-00ba16543895",
            "compositeImage": {
                "id": "79bbc5c9-0406-4afa-a580-486812a9b29d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b34cdd5-668c-45c0-af8e-c2d8a840a16a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a14f61a9-82c5-4258-a79b-fe5bee6bacb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b34cdd5-668c-45c0-af8e-c2d8a840a16a",
                    "LayerId": "10342ab4-1203-4c2f-8598-6442e009193e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "10342ab4-1203-4c2f-8598-6442e009193e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eef9b7f7-d0aa-42a2-9f37-00ba16543895",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}